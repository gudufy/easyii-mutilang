<?php
use yeesoft\multilingual\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $model->title;
?>
<?= $this->render('_menu') ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<?php $form = ActiveForm::begin(['enableAjaxValidation' => true]) ?>
<?= $form->field($formModel, 'title_zh_cn') ?>
<?= $form->field($formModel, 'title_en_us') ?>
<?= $form->field($formModel, 'name') ?>
<?= Html::submitButton(Yii::t('easyii', 'Copy'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>