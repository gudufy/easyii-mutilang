<?php
namespace yii\easyii\models;

use Yii;
use yii\base\Model;

class CopyModuleForm extends Model
{
    public $title_zh_cn;
    public $title_en_us;
    public $name;

    public function rules()
    {
        return [
            [['title_zh_cn','title_en_us', 'name'], 'required'],
            ['name', 'match', 'pattern' => '/^[\w]+$/'],
            ['name', 'unique', 'targetClass' => Module::className()],
        ];
    }
    public function attributeLabels()
    {
        return [
            'name' => 'New module name',
            'title_zh_cn' => Yii::t('easyii', 'Title').' [简体中文]',
            'title_en_us' => Yii::t('easyii', 'Title').' [English]',
        ];
    }
}