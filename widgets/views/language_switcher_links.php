<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
?>

<?php foreach ($languages as $key => $lang) : ?>
        <?php $title = ($display == 'code') ? $key : $lang; ?>

        <?php if ($language == $key) : ?>
            <li class="active"><a href="#"><?= $title ?></a></li>
        <?php else: ?>
            <li><?= Html::a($title, Yii::$app->urlManager->createUrl(ArrayHelper::merge($params, [$url, "language" => $key]))) ?></li>
        <?php endif; ?>

    <?php endforeach; ?>