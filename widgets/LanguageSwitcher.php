<?php
namespace yii\easyii\widgets;

use Yii;
use yii\base\Widget;

class LanguageSwitcher extends \yeesoft\multilingual\widgets\LanguageSwitcher
{
    public function init()
    {
        parent::init();

        $this->_reservedViews = [
            'links' => 'language_switcher_links',
        ];

        $this->view = LanguageSwitcher::VIEW_LINKS;
    }
}