<?php
use yii\db\Schema;
use yii\easyii\models;

use yii\easyii\modules\catalog;
use yii\easyii\modules\shopcart;
use yii\easyii\modules\article;
use yii\easyii\modules\carousel\models\Carousel;
use yii\easyii\modules\faq\models\Faq;
use yii\easyii\modules\feedback\models\Feedback;
use yii\easyii\modules\file\models\File;
use yii\easyii\modules\gallery;
use yii\easyii\modules\guestbook\models\Guestbook;
use yii\easyii\modules\news\models\News;
use yii\easyii\modules\page\models\Page;
use yii\easyii\modules\subscribe\models\Subscriber;
use yii\easyii\modules\subscribe\models\History;
use yii\easyii\modules\text\models\Text;

class m000000_000000_install extends \yii\db\Migration
{
    const VERSION = 0.9;

    public $engine = 'ENGINE=MyISAM DEFAULT CHARSET=utf8';
    
    public function up()
    {
		$this->execute('SET foreign_key_checks = 0');
 
        $this->createTable('{{%easyii_admins}}', [
            'admin_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'username' => 'VARCHAR(32) NOT NULL',
            'password' => 'VARCHAR(64) NOT NULL',
            'auth_key' => 'VARCHAR(128) NOT NULL',
            'access_token' => 'VARCHAR(128) NULL',
            'PRIMARY KEY (`admin_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('access_token','{{%easyii_admins}}','access_token',1);
        
        $this->createTable('{{%easyii_article_categories}}', [
            'category_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(128) NOT NULL',
            'image' => 'VARCHAR(128) NULL',
            'order_num' => 'INT(11) NULL',
            'slug' => 'VARCHAR(128) NULL',
            'tree' => 'INT(11) NULL',
            'lft' => 'INT(11) NULL',
            'rgt' => 'INT(11) NULL',
            'depth' => 'INT(11) NULL',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`category_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_article_categories}}','slug',1);
        
        $this->createTable('{{%easyii_article_categories_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_article_categories_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_article_categories_lang}}','language',0);
        
        $this->createTable('{{%easyii_article_items}}', [
            'item_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'category_id' => 'INT(11) NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'image' => 'VARCHAR(128) NULL',
            'short' => 'VARCHAR(1024) NULL',
            'text' => 'TEXT NOT NULL',
            'slug' => 'VARCHAR(128) NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'views' => 'INT(11) NULL DEFAULT \'0\'',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`item_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_article_items}}','slug',1);
        
        $this->createTable('{{%easyii_article_items_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'short' => 'VARCHAR(1024) NULL',
            'text' => 'TEXT NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_article_items_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_article_items_lang}}','language',0);
        
        $this->createTable('{{%easyii_carousel}}', [
            'carousel_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'image' => 'VARCHAR(128) NOT NULL',
            'link' => 'VARCHAR(255) NOT NULL',
            'title' => 'VARCHAR(128) NULL',
            'text' => 'TEXT NULL',
            'order_num' => 'INT(11) NULL',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`carousel_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_carousel_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'link' => 'VARCHAR(255) NOT NULL',
            'title' => 'VARCHAR(128) NULL',
            'text' => 'TEXT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_carousel_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_carousel_lang}}','language',0);
        
        $this->createTable('{{%easyii_catalog_categories}}', [
            'category_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(128) NOT NULL',
            'image' => 'VARCHAR(128) NULL',
            'fields' => 'TEXT NOT NULL',
            'slug' => 'VARCHAR(128) NULL',
            'tree' => 'INT(11) NULL',
            'lft' => 'INT(11) NULL',
            'rgt' => 'INT(11) NULL',
            'depth' => 'INT(11) NULL',
            'order_num' => 'INT(11) NULL',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`category_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_catalog_categories}}','slug',1);
        
        $this->createTable('{{%easyii_catalog_categories_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_catalog_categories_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_catalog_categories_lang}}','language',0);
        
        $this->createTable('{{%easyii_catalog_item_data}}', [
            'data_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'item_id' => 'INT(11) NULL',
            'name' => 'VARCHAR(128) NOT NULL',
            'value' => 'VARCHAR(1024) NULL',
            'PRIMARY KEY (`data_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('item_id_name','{{%easyii_catalog_item_data}}','item_id, name',0);
        $this->createIndex('value','{{%easyii_catalog_item_data}}','value',0);
        
        $this->createTable('{{%easyii_catalog_items}}', [
            'item_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'category_id' => 'INT(11) NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'description' => 'TEXT NULL',
            'available' => 'INT(11) NULL DEFAULT \'1\'',
            'price' => 'FLOAT NULL',
            'discount' => 'INT(11) NULL DEFAULT \'0\'',
            'data' => 'TEXT NOT NULL',
            'image' => 'VARCHAR(128) NULL',
            'slug' => 'VARCHAR(128) NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`item_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_catalog_items}}','slug',1);
        
        $this->createTable('{{%easyii_catalog_items_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'description' => 'TEXT NULL',
            'available' => 'INT(11) NULL DEFAULT \'1\'',
            'price' => 'FLOAT NULL',
            'discount' => 'INT(11) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_catalog_items_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_catalog_items_lang}}','language',0);
        
        $this->createTable('{{%easyii_faq}}', [
            'faq_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'question' => 'TEXT NOT NULL',
            'answer' => 'TEXT NOT NULL',
            'order_num' => 'INT(11) NULL',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`faq_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_faq_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'question' => 'TEXT NOT NULL',
            'answer' => 'TEXT NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_faq_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_faq_lang}}','language',0);
        
        $this->createTable('{{%easyii_feedback}}', [
            'feedback_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'email' => 'VARCHAR(128) NOT NULL',
            'phone' => 'VARCHAR(64) NULL',
            'title' => 'VARCHAR(128) NULL',
            'text' => 'TEXT NOT NULL',
            'answer_subject' => 'VARCHAR(128) NULL',
            'answer_text' => 'TEXT NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'ip' => 'VARCHAR(16) NOT NULL',
            'status' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`feedback_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_files}}', [
            'file_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(128) NOT NULL',
            'file' => 'VARCHAR(255) NOT NULL',
            'size' => 'INT(11) NOT NULL',
            'slug' => 'VARCHAR(128) NULL',
            'downloads' => 'INT(11) NULL DEFAULT \'0\'',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'order_num' => 'INT(11) NULL',
            'PRIMARY KEY (`file_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_files}}','slug',1);
        
        $this->createTable('{{%easyii_files_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_files_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_files_lang}}','language',0);
        
        $this->createTable('{{%easyii_gallery_categories}}', [
            'category_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(128) NOT NULL',
            'image' => 'VARCHAR(128) NULL',
            'slug' => 'VARCHAR(128) NULL',
            'tree' => 'INT(11) NULL',
            'lft' => 'INT(11) NULL',
            'rgt' => 'INT(11) NULL',
            'depth' => 'INT(11) NULL',
            'order_num' => 'INT(11) NULL',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`category_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_gallery_categories}}','slug',1);
        
        $this->createTable('{{%easyii_gallery_categories_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_gallery_categories_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_gallery_categories_lang}}','language',0);
        
        $this->createTable('{{%easyii_guestbook}}', [
            'guestbook_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(128) NOT NULL',
            'title' => 'VARCHAR(128) NULL',
            'text' => 'TEXT NOT NULL',
            'answer' => 'TEXT NULL',
            'email' => 'VARCHAR(128) NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'ip' => 'VARCHAR(16) NOT NULL',
            'new' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'status' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`guestbook_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_loginform}}', [
            'log_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'username' => 'VARCHAR(128) NOT NULL',
            'password' => 'VARCHAR(128) NOT NULL',
            'ip' => 'VARCHAR(16) NOT NULL',
            'user_agent' => 'VARCHAR(1024) NOT NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'success' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`log_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_modules}}', [
            'module_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'class' => 'VARCHAR(128) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'icon' => 'VARCHAR(32) NOT NULL',
            'settings' => 'TEXT NOT NULL',
            'notice' => 'INT(11) NULL DEFAULT \'0\'',
            'order_num' => 'INT(11) NULL',
            'status' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`module_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('name','{{%easyii_modules}}','name',1);
        
        $this->createTable('{{%easyii_modules_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_modules_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_modules_lang}}','language',0);
        
        $this->createTable('{{%easyii_news}}', [
            'news_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(128) NOT NULL',
            'image' => 'VARCHAR(128) NULL',
            'short' => 'VARCHAR(1024) NULL',
            'text' => 'TEXT NOT NULL',
            'slug' => 'VARCHAR(128) NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'views' => 'INT(11) NULL DEFAULT \'0\'',
            'status' => 'TINYINT(1) NULL DEFAULT \'1\'',
            'PRIMARY KEY (`news_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_news}}','slug',1);
        
        $this->createTable('{{%easyii_news_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'short' => 'VARCHAR(1024) NULL',
            'text' => 'TEXT NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_news_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_news_lang}}','language',0);
        
        $this->createTable('{{%easyii_pages}}', [
            'page_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'title' => 'VARCHAR(128) NOT NULL',
            'text' => 'TEXT NOT NULL',
            'slug' => 'VARCHAR(128) NULL',
            'PRIMARY KEY (`page_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_pages}}','slug',1);
        
        $this->createTable('{{%easyii_pages_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'text' => 'TEXT NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_pages_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_pages_lang}}','language',0);
        
        $this->createTable('{{%easyii_photos}}', [
            'photo_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'class' => 'VARCHAR(128) NOT NULL',
            'item_id' => 'INT(11) NOT NULL',
            'image' => 'VARCHAR(128) NOT NULL',
            'description' => 'VARCHAR(1024) NOT NULL',
            'order_num' => 'INT(11) NOT NULL',
            'PRIMARY KEY (`photo_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('model_item','{{%easyii_photos}}','class, item_id',0);
        
        $this->createTable('{{%easyii_seotext}}', [
            'seotext_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'class' => 'VARCHAR(128) NOT NULL',
            'item_id' => 'INT(11) NOT NULL',
            'h1' => 'VARCHAR(128) NULL',
            'title' => 'VARCHAR(128) NULL',
            'keywords' => 'VARCHAR(128) NULL',
            'description' => 'VARCHAR(128) NULL',
            'PRIMARY KEY (`seotext_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('model_item','{{%easyii_seotext}}','class, item_id',1);
        
        $this->createTable('{{%easyii_seotext_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'h1' => 'VARCHAR(128) NULL',
            'title' => 'VARCHAR(128) NULL',
            'keywords' => 'VARCHAR(128) NULL',
            'description' => 'VARCHAR(128) NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_seotext_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_seotext_lang}}','language',0);
        
        $this->createTable('{{%easyii_settings}}', [
            'setting_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'title' => 'VARCHAR(128) NOT NULL',
            'value' => 'VARCHAR(1024) NOT NULL',
            'visibility' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`setting_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('name','{{%easyii_settings}}','name',1);
        
        $this->createTable('{{%easyii_shopcart_goods}}', [
            'good_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'order_id' => 'INT(11) NULL',
            'item_id' => 'INT(11) NULL',
            'count' => 'INT(11) NULL',
            'options' => 'VARCHAR(255) NOT NULL',
            'price' => 'FLOAT NULL',
            'discount' => 'INT(11) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`good_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_shopcart_orders}}', [
            'order_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(64) NOT NULL',
            'address' => 'VARCHAR(255) NOT NULL',
            'phone' => 'VARCHAR(64) NOT NULL',
            'email' => 'VARCHAR(128) NOT NULL',
            'comment' => 'VARCHAR(1024) NOT NULL',
            'remark' => 'VARCHAR(1024) NOT NULL',
            'access_token' => 'VARCHAR(32) NOT NULL',
            'ip' => 'VARCHAR(16) NOT NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'new' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'status' => 'TINYINT(1) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`order_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_subscribe_history}}', [
            'history_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'subject' => 'VARCHAR(128) NOT NULL',
            'body' => 'TEXT NOT NULL',
            'sent' => 'INT(11) NULL DEFAULT \'0\'',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`history_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createTable('{{%easyii_subscribe_subscribers}}', [
            'subscriber_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'email' => 'VARCHAR(128) NOT NULL',
            'ip' => 'VARCHAR(16) NOT NULL',
            'time' => 'INT(11) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`subscriber_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('email','{{%easyii_subscribe_subscribers}}','email',1);
        
        $this->createTable('{{%easyii_tags}}', [
            'tag_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(128) NOT NULL',
            'frequency' => 'INT(11) NULL DEFAULT \'0\'',
            'PRIMARY KEY (`tag_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('name','{{%easyii_tags}}','name',1);
        
        $this->createTable('{{%easyii_tags_assign}}', [
            'class' => 'VARCHAR(128) NOT NULL',
            'item_id' => 'INT(11) NOT NULL',
            'tag_id' => 'INT(11) NOT NULL',
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('class','{{%easyii_tags_assign}}','class',0);
        $this->createIndex('item_tag','{{%easyii_tags_assign}}','item_id, tag_id',0);
        
        $this->createTable('{{%easyii_texts}}', [
            'text_id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'text' => 'TEXT NOT NULL',
            'slug' => 'VARCHAR(128) NULL',
            'PRIMARY KEY (`text_id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('slug','{{%easyii_texts}}','slug',1);
        
        $this->createTable('{{%easyii_texts_lang}}', [
            'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
            'owner_id' => 'INT(11) NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'text' => 'TEXT NOT NULL',
            'PRIMARY KEY (`id`)'
        ], "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB");
        
        $this->createIndex('owner_id','{{%easyii_texts_lang}}','owner_id',0);
        $this->createIndex('language','{{%easyii_texts_lang}}','language',0);
        
        
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        
        /* Table easyii_admins */
        $this->batchInsert('{{%easyii_admins}}',['admin_id','username','password','auth_key','access_token'],[]);
        
        /* Table easyii_article_categories */
        $this->batchInsert('{{%easyii_article_categories}}',['category_id','title','image','order_num','slug','tree','lft','rgt','depth','status'],[['1','Articles category 1',null,'2','wen-zhang-fen-lei1','1','1','2','0','1'],
        ['2','Articles category 2',null,'1','articles-category-2','2','1','6','0','1'],
        ['3','Subcategory 1',null,'1','subcategory-1','2','2','3','1','1'],
        ['4','Subcategory 2',null,'1','subcategory-2','2','4','5','1','1'],
        ]);
        
        /* Table easyii_article_categories_lang */
        $this->batchInsert('{{%easyii_article_categories_lang}}',['id','owner_id','language','title'],[['5','1','zh-CN','文章分类1'],
        ['6','1','en-US','Articles category 1'],
        ['7','4','zh-CN','二级分类2'],
        ['8','4','en-US','Subcategory 2'],
        ]);
        
        /* Table easyii_article_items */
        $this->batchInsert('{{%easyii_article_items}}',['item_id','category_id','title','image','short','text','slug','time','views','status'],[['1','1','第一条新闻标题','/uploads/article/article-1.jpg','第一条新闻简介','<p>第一条新闻内容</p>','di-yi-tiao-xin-wen-biao-ti','1491897356','4','1'],
        ['2','1','Second article title','/uploads/article/article-2.jpg','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p><ol> <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </li><li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</li></ol>','second-article-title','1491897356','0','1'],
        ['3','1','Third article title','/uploads/article/article-3.jpg','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>','third-article-title','1491810956','0','1'],
        ]);
        
        /* Table easyii_article_items_lang */
        $this->batchInsert('{{%easyii_article_items_lang}}',['id','owner_id','language','title','short','text'],[['4','1','zh-CN','第一条文章标题','第一条文章简介','<p>第一条文章内容</p>'],
        ['5','1','en-US','First article title','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p><strong style="background-color: initial;">Sed ut perspiciatis</strong>, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><ul> <li>item 1</li><li>item 2</li><li>item 3</li></ul><p>ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?</p>'],
        ]);
        
        /* Table easyii_carousel */
        $this->batchInsert('{{%easyii_carousel}}',['carousel_id','image','link','title','text','order_num','status'],[['1','/uploads/carousel/1.jpg','','Ut enim ad minim veniam, quis nostrud exercitation','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.','1','1'],
        ['2','/uploads/carousel/2.jpg','','这里是大图标题','这里是很长很长的大图描述','2','1'],
        ['3','/uploads/carousel/3.jpg','','Lorem ipsum dolor sit amet, consectetur adipiscing elit','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.','3','1'],
        ]);
        
        /* Table easyii_carousel_lang */
        $this->batchInsert('{{%easyii_carousel_lang}}',['id','owner_id','language','link','title','text'],[['4','2','en-US','','Sed do eiusmod tempor incididunt ut labore et','Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.'],
        ['5','2','zh-CN','','这里是大图标题','这里是很长很长的大图描述'],
        ['6','3','en-US','','',''],
        ['7','3','zh-CN','','',''],
        ]);
        
        /* Table easyii_catalog_categories */
        $this->batchInsert('{{%easyii_catalog_categories}}',['category_id','title','image','fields','slug','tree','lft','rgt','depth','order_num','status'],[['1','Gadgets',null,'[{"name":"brand","title":"Brand","type":"select","options":["Samsung","Apple","Nokia"]},{"name":"storage","title":"Storage","type":"string","options":""},{"name":"touchscreen","title":"Touchscreen","type":"boolean","options":""},{"name":"cpu","title":"CPU cores","type":"select","options":["1","2","4","8"]},{"name":"features","title":"Features","type":"checkbox","options":["Wi-fi","4G","GPS"]},{"name":"color","title":"Color","type":"checkbox","options":["White","Black","Red","Blue"]}]','gadgets','1','1','6','0',null,'1'],
        ['2','Smartphones',null,'[{"name":"brand","title":"Brand","type":"select","options":["Samsung","Apple","Nokia"]},{"name":"storage","title":"Storage","type":"string","options":""},{"name":"touchscreen","title":"Touchscreen","type":"boolean","options":""},{"name":"cpu","title":"CPU cores","type":"select","options":["1","2","4","8"]},{"name":"features","title":"Features","type":"checkbox","options":["Wi-fi","4G","GPS"]},{"name":"color","title":"Color","type":"checkbox","options":["White","Black","Red","Blue"]}]','smartphones','1','2','3','1',null,'1'],
        ['3','Tablets',null,'[{"name":"brand","title":"Brand","type":"select","options":["Samsung","Apple","Nokia"]},{"name":"storage","title":"Storage","type":"string","options":""},{"name":"touchscreen","title":"Touchscreen","type":"boolean","options":""},{"name":"cpu","title":"CPU cores","type":"select","options":["1","2","4","8"]},{"name":"features","title":"Features","type":"checkbox","options":["Wi-fi","4G","GPS"]},{"name":"color","title":"Color","type":"checkbox","options":["White","Black","Red","Blue"]}]','tablets','1','4','5','1',null,'1'],
        ]);
        
        /* Table easyii_catalog_categories_lang */
        $this->batchInsert('{{%easyii_catalog_categories_lang}}',['id','owner_id','language','title'],[['5','2','zh-CN','智能手机'],
        ['6','2','en-US','Smartphones'],
        ]);
        
        /* Table easyii_catalog_item_data */
        $this->batchInsert('{{%easyii_catalog_item_data}}',['data_id','item_id','name','value'],[['25','1','color','White'],
        ['24','1','cpu','1'],
        ['23','1','touchscreen','0'],
        ['22','1','storage','1'],
        ['21','1','brand','Nokia'],
        ['8','2','brand','Samsung'],
        ['9','2','storage','32'],
        ['10','2','touchscreen','1'],
        ['11','2','cpu','8'],
        ['12','2','features','Wi-fi'],
        ['13','2','features','GPS'],
        ['14','3','brand','Apple'],
        ['15','3','storage','64'],
        ['16','3','touchscreen','1'],
        ['17','3','cpu','4'],
        ['18','3','features','Wi-fi'],
        ['19','3','features','4G'],
        ['20','3','features','GPS'],
        ['26','1','color','Red'],
        ['27','1','color','Blue'],
        ]);
        
        /* Table easyii_catalog_items */
        $this->batchInsert('{{%easyii_catalog_items}}',['item_id','category_id','title','description','available','price','discount','data','image','slug','time','status'],[['1','2','Nokia 3310','<h3>The legend</h3><p>The Nokia 3310 is a GSMmobile phone announced on September 1, 2000, and released in the fourth quarter of the year, replacing the popular Nokia 3210. The phone sold extremely well, being one of the most successful phones with 126 million units sold worldwide.&nbsp;The phone has since received a cult status and is still widely acclaimed today.</p><p>The 3310 was developed at the Copenhagen Nokia site in Denmark. It is a compact and sturdy phone featuring an 84 × 48 pixel pure monochrome display. It has a lighter 115 g battery variant which has fewer features; for example the 133 g battery version has the start-up image of two hands touching while the 115 g version does not. It is a slightly rounded rectangular unit that is typically held in the palm of a hand, with the buttons operated with the thumb. The blue button is the main button for selecting options, with "C" button as a "back" or "undo" button. Up and down buttons are used for navigation purposes. The on/off/profile button is a stiff black button located on the top of the phone.</p>','5','100','0','{"brand":"Nokia","storage":"1","touchscreen":"0","cpu":"1","color":["White","Red","Blue"]}','/uploads/catalog/3310.jpg','nokia-3310','1491983756','1'],
        ['2','2','Galaxy S6','<h3>Next is beautifully crafted</h3><p>With their slim, seamless, full metal and glass construction, the sleek, ultra thin edged Galaxy S6 and unique, dual curved Galaxy S6 edge are crafted from the finest materials.</p><p>And while they may be the thinnest smartphones we`ve ever created, when it comes to cutting-edge technology and flagship Galaxy experience, these 5.1" QHD Super AMOLED smartphones are certainly no lightweights.</p>','1','1000','10','{"brand":"Samsung","storage":"32","touchscreen":"1","cpu":8,"features":["Wi-fi","GPS"]}','/uploads/catalog/galaxy.jpg','galaxy-s6','1491897356','1'],
        ['3','2','Iphone 6','<h3>Next is beautifully crafted</h3><p>With their slim, seamless, full metal and glass construction, the sleek, ultra thin edged Galaxy S6 and unique, dual curved Galaxy S6 edge are crafted from the finest materials.</p><p>And while they may be the thinnest smartphones we`ve ever created, when it comes to cutting-edge technology and flagship Galaxy experience, these 5.1" QHD Super AMOLED smartphones are certainly no lightweights.</p>','0','1100','10','{"brand":"Apple","storage":"64","touchscreen":"1","cpu":4,"features":["Wi-fi","4G","GPS"]}','/uploads/catalog/iphone.jpg','iphone-6','1491810956','1'],
        ]);
        
        /* Table easyii_catalog_items_lang */
        $this->batchInsert('{{%easyii_catalog_items_lang}}',['id','owner_id','language','title','description','available','price','discount'],[['4','1','zh-CN','Nokia(诺基亚) 3310','','5','680',null],
        ['5','1','en-US','Nokia 3310','','5','100',null],
        ]);
        
        /* Table easyii_faq */
        $this->batchInsert('{{%easyii_faq}}',['faq_id','question','answer','order_num','status'],[['1','Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it?','But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure','1','1'],
        ['2','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum?','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta <a href="http://easyiicms.com/">sunt explicabo</a>.','2','1'],
        ['3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','t enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.','3','1'],
        ]);
        
        /* Table easyii_faq_lang */
        $this->batchInsert('{{%easyii_faq_lang}}',['id','owner_id','language','question','answer'],[['4','3','zh-CN','问题1','答案1'],
        ['5','3','en-US','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','t enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'],
        ]);
        
        /* Table easyii_feedback */
        $this->batchInsert('{{%easyii_feedback}}',['feedback_id','name','email','phone','title','text','answer_subject','answer_text','time','ip','status'],[]);
        
        /* Table easyii_files */
        $this->batchInsert('{{%easyii_files}}',['file_id','title','file','size','slug','downloads','time','order_num'],[['1','报价','/uploads/files/example.csv','104','bao-jia','0','1491983759','1'],
        ]);
        
        /* Table easyii_files_lang */
        $this->batchInsert('{{%easyii_files_lang}}',['id','owner_id','language','title'],[['2','1','en-US','Download price list'],
        ['3','1','zh-CN','报价下载'],
        ]);
        
        /* Table easyii_gallery_categories */
        $this->batchInsert('{{%easyii_gallery_categories}}',['category_id','title','image','slug','tree','lft','rgt','depth','order_num','status'],[['1','Album 1','/uploads/gallery/album-1.jpg','album-1','1','1','2','0','2','1'],
        ['2','Album 2','/uploads/gallery/album-2.jpg','album-2','2','1','2','0','1','1'],
        ]);
        
        /* Table easyii_gallery_categories_lang */
        $this->batchInsert('{{%easyii_gallery_categories_lang}}',['id','owner_id','language','title'],[['5','1','zh-CN','相册1'],
        ['6','1','en-US','Album 1'],
        ]);
        
        /* Table easyii_guestbook */
        $this->batchInsert('{{%easyii_guestbook}}',['guestbook_id','name','title','text','answer','email','time','ip','new','status'],[['1','First user','','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',null,null,'1491983756','127.0.0.1','0','1'],
        ['2','Second user','','Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',null,'1491983759','127.0.0.1','0','1'],
        ['3','Third user','','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',null,null,'1491983759','127.0.0.1','0','1'],
        ]);
        
        /* Table easyii_loginform */
        $this->batchInsert('{{%easyii_loginform}}',['log_id','username','password','ip','user_agent','time','success'],[['1','root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36','1491983755','1'],
        ['2','root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36','1492046422','1'],
        ['3','root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36','1492049705','1'],
        ]);
        
        /* Table easyii_modules */
        $this->batchInsert('{{%easyii_modules}}',['module_id','name','class','title','icon','settings','notice','order_num','status'],[['1','article','yii\easyii\modules\article\ArticleModule','Articles','pencil','{"categoryThumb":true,"articleThumb":true,"enablePhotos":true,"enableShort":true,"shortMaxLength":255,"enableTags":true,"itemsInFolder":false}','0','65','1'],
        ['2','carousel','yii\easyii\modules\carousel\CarouselModule','焦点图','picture','{"enableTitle":true,"enableText":true}','0','40','1'],
        ['3','catalog','yii\easyii\modules\catalog\CatalogModule','商品','list-alt','{"categoryThumb":true,"itemsInFolder":false,"itemThumb":true,"itemPhotos":true,"itemDescription":true,"itemSale":true}','0','100','1'],
        ['4','faq','yii\easyii\modules\faq\FaqModule','FAQ','question-sign','[]','0','45','1'],
        ['5','feedback','yii\easyii\modules\feedback\FeedbackModule','Feedback','earphone','{"mailAdminOnNewFeedback":true,"subjectOnNewFeedback":"New feedback","templateOnNewFeedback":"@easyii\/modules\/feedback\/mail\/en\/new_feedback","answerTemplate":"@easyii\/modules\/feedback\/mail\/en\/answer","answerSubject":"Answer on your feedback message","answerHeader":"Hello,","answerFooter":"Best regards.","enableTitle":false,"enablePhone":true,"enableCaptcha":false}','0','60','1'],
        ['6','file','yii\easyii\modules\file\FileModule','Files','floppy-disk','[]','0','30','1'],
        ['7','gallery','yii\easyii\modules\gallery\GalleryModule','Photo Gallery','camera','{"categoryThumb":true,"itemsInFolder":false}','0','90','1'],
        ['8','guestbook','yii\easyii\modules\guestbook\GuestbookModule','Guestbook','book','{"enableTitle":false,"enableEmail":true,"preModerate":false,"enableCaptcha":false,"mailAdminOnNewPost":true,"subjectOnNewPost":"New message in the guestbook.","templateOnNewPost":"@easyii\/modules\/guestbook\/mail\/en\/new_post","frontendGuestbookRoute":"\/guestbook","subjectNotifyUser":"Your post in the guestbook answered","templateNotifyUser":"@easyii\/modules\/guestbook\/mail\/en\/notify_user"}','0','80','1'],
        ['9','news','yii\easyii\modules\news\NewsModule','News','bullhorn','{"enableThumb":true,"enablePhotos":true,"enableShort":true,"shortMaxLength":256,"enableTags":true}','0','70','1'],
        ['10','page','yii\easyii\modules\page\PageModule','Pages','file','[]','0','50','1'],
        ['11','shopcart','yii\easyii\modules\shopcart\ShopcartModule','Orders','shopping-cart','{"mailAdminOnNewOrder":true,"subjectOnNewOrder":"New order","templateOnNewOrder":"@easyii\/modules\/shopcart\/mail\/en\/new_order","subjectNotifyUser":"Your order status changed","templateNotifyUser":"@easyii\/modules\/shopcart\/mail\/en\/notify_user","frontendShopcartRoute":"\/shopcart\/order","enablePhone":true,"enableEmail":true}','0','110','1'],
        ['12','subscribe','yii\easyii\modules\subscribe\SubscribeModule','E-mail subscribe','envelope','[]','0','10','1'],
        ['13','text','yii\easyii\modules\text\TextModule','Text blocks','font','[]','0','20','1'],
        ]);
        
        /* Table easyii_modules_lang */
        $this->batchInsert('{{%easyii_modules_lang}}',['id','owner_id','language','title'],[['14','11','en-US','Orders'],
        ['15','11','zh-CN','订单'],
        ['16','7','en-US','Photo Gallery'],
        ['17','7','zh-CN','相册'],
        ['18','3','en-US','Catalog'],
        ['19','3','zh-CN','商品'],
        ['20','8','en-US','Guestbook'],
        ['21','8','zh-CN','留言'],
        ['22','9','en-US','News'],
        ['23','9','zh-CN','新闻'],
        ['24','1','en-US','Articles'],
        ['25','1','zh-CN','文章'],
        ['26','5','en-US','Feedback'],
        ['27','5','zh-CN','反馈'],
        ['28','10','en-US','Pages'],
        ['29','10','zh-CN','页面'],
        ['30','2','en-US','Carousel'],
        ['31','2','zh-CN','首页焦点图'],
        ['32','6','en-US','Files'],
        ['33','6','zh-CN','文件'],
        ['34','13','en-US','Text blocks'],
        ['35','13','zh-CN','文本块'],
        ['36','12','en-US','E-mail subscribe'],
        ['37','12','zh-CN','邮件订阅'],
        ]);
        
        /* Table easyii_news */
        $this->batchInsert('{{%easyii_news}}',['news_id','title','image','short','text','slug','time','views','status'],[['1','First news title','/uploads/news/news-1.jpg','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p><strong>Sed ut perspiciatis</strong>, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.&nbsp;</p><ul><li>item 1</li><li>item 2</li><li>item 3</li></ul><p>ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?</p>','first-news-title','1491983756','3','1'],
        ['2','Second news title','/uploads/news/news-2.jpg','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p><ol> <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </li><li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</li></ol>','second-news-title','1491897356','0','1'],
        ['3','Third news title','/uploads/news/news-3.jpg','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>','third-news-title','1491810956','0','1'],
        ]);
        
        /* Table easyii_news_lang */
        $this->batchInsert('{{%easyii_news_lang}}',['id','owner_id','language','title','short','text'],[['4','1','zh-CN','第一条新闻标题','第一条新闻简述','<p>第一条新闻内容</p>'],
        ['5','1','en-US','First news title','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt molliti','<p><strong style="background-color: initial;">Sed ut perspiciatis</strong>, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p><ul> <li>item 1</li><li>item 2</li><li>item 3</li></ul><p>ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?</p>'],
        ]);
        
        /* Table easyii_pages */
        $this->batchInsert('{{%easyii_pages}}',['page_id','title','text','slug'],[['1','Index','<p><strong>All elements are live-editable, switch on Live Edit button to see this feature.</strong>&nbsp;</p><p>Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>','page-index'],
        ['2','Shop','','page-shop'],
        ['3','Shop search','','page-shop-search'],
        ['4','Shopping cart','','page-shopcart'],
        ['5','Order created','<p>Your order successfully created. Our manager will contact you as soon as possible.</p>','page-shopcart-success'],
        ['6','News','','page-news'],
        ['7','Articles','','page-articles'],
        ['8','Gallery','','page-gallery'],
        ['9','Guestbook','','page-guestbook'],
        ['10','FAQ','','page-faq'],
        ['11','联系我们','<p><strong>Address</strong>: Dominican republic, Santo Domingo, Some street 123</p><p><strong>ZIP</strong>: 123456</p><p><strong>Phone</strong>: +1 234 56-78</p><p><strong>E-mail</strong>: demo@example.com</p>','page-contact'],
        ]);
        
        /* Table easyii_pages_lang */
        $this->batchInsert('{{%easyii_pages_lang}}',['id','owner_id','language','title','text'],[['12','1','en-US','Index','<p><strong>All elements are live-editable, switch on Live Edit button to see this feature.</strong>&nbsp;</p><p>Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'],
        ['14','1','zh-CN','首页','<p>这是中文介绍</p>'],
        ]);
        
        /* Table easyii_photos */
        $this->batchInsert('{{%easyii_photos}}',['photo_id','class','item_id','image','description','order_num'],[['1','yii\easyii\modules\catalog\models\Item','1','/uploads/photos/3310-1.jpg','','1'],
        ['2','yii\easyii\modules\catalog\models\Item','1','/uploads/photos/3310-2.jpg','','2'],
        ['3','yii\easyii\modules\catalog\models\Item','2','/uploads/photos/galaxy-1.jpg','','3'],
        ['4','yii\easyii\modules\catalog\models\Item','2','/uploads/photos/galaxy-2.jpg','','4'],
        ['5','yii\easyii\modules\catalog\models\Item','2','/uploads/photos/galaxy-3.jpg','','5'],
        ['6','yii\easyii\modules\catalog\models\Item','2','/uploads/photos/galaxy-4.jpg','','6'],
        ['7','yii\easyii\modules\catalog\models\Item','3','/uploads/photos/iphone-1.jpg','','7'],
        ['8','yii\easyii\modules\catalog\models\Item','3','/uploads/photos/iphone-2.jpg','','8'],
        ['9','yii\easyii\modules\catalog\models\Item','3','/uploads/photos/iphone-3.jpg','','9'],
        ['10','yii\easyii\modules\catalog\models\Item','3','/uploads/photos/iphone-4.jpg','','10'],
        ['11','yii\easyii\modules\news\models\News','1','/uploads/photos/news-1-1.jpg','','11'],
        ['12','yii\easyii\modules\news\models\News','1','/uploads/photos/news-1-2.jpg','','12'],
        ['13','yii\easyii\modules\news\models\News','1','/uploads/photos/news-1-3.jpg','','13'],
        ['14','yii\easyii\modules\news\models\News','1','/uploads/photos/news-1-4.jpg','','14'],
        ['15','yii\easyii\modules\article\models\Item','1','/uploads/photos/article-1-1.jpg','','15'],
        ['16','yii\easyii\modules\article\models\Item','1','/uploads/photos/article-1-2.jpg','','16'],
        ['17','yii\easyii\modules\article\models\Item','1','/uploads/photos/article-1-3.jpg','','17'],
        ['18','yii\easyii\modules\article\models\Item','1','/uploads/photos/news-1-4.jpg','','18'],
        ['19','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-9.jpg','','19'],
        ['20','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-8.jpg','','20'],
        ['21','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-7.jpg','','21'],
        ['22','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-6.jpg','','22'],
        ['23','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-5.jpg','','23'],
        ['24','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-4.jpg','','24'],
        ['25','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-3.jpg','','25'],
        ['26','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-2.jpg','','26'],
        ['27','yii\easyii\modules\gallery\models\Category','1','/uploads/photos/album-1-1.jpg','','27'],
        ]);
        
        /* Table easyii_seotext */
        $this->batchInsert('{{%easyii_seotext}}',['seotext_id','class','item_id','h1','title','keywords','description'],[['1','yii\easyii\modules\page\models\Page','1','','EasyiiCMS demo a','','yii2, easyii, admin b'],
        ['2','yii\easyii\modules\page\models\Page','2','Shop categories','Extended shop title','',''],
        ['3','yii\easyii\modules\page\models\Page','3','Shop search results','Extended shop search title','',''],
        ['4','yii\easyii\modules\page\models\Page','4','Shopping cart H1','Extended shopping cart title','',''],
        ['5','yii\easyii\modules\page\models\Page','5','Success','Extended order success title','',''],
        ['6','yii\easyii\modules\page\models\Page','6','News H1','Extended news title','',''],
        ['7','yii\easyii\modules\page\models\Page','7','Articles H1','Extended articles title','',''],
        ['8','yii\easyii\modules\page\models\Page','8','Photo gallery','Extended gallery title','',''],
        ['9','yii\easyii\modules\page\models\Page','9','Guestbook H1','Extended guestbook title','',''],
        ['10','yii\easyii\modules\page\models\Page','10','Frequently Asked Question','Extended faq title','',''],
        ['11','yii\easyii\modules\page\models\Page','11','Contact us','Extended contact title','',''],
        ['12','yii\easyii\modules\catalog\models\Category','2','Smartphones H1','Extended smartphones title','',''],
        ['13','yii\easyii\modules\catalog\models\Category','3','Tablets H1','Extended tablets title','',''],
        ['14','yii\easyii\modules\catalog\models\Item','1','Nokia 3310','','',''],
        ['15','yii\easyii\modules\catalog\models\Item','2','Samsung Galaxy S6','','',''],
        ['16','yii\easyii\modules\catalog\models\Item','3','Apple Iphone 6','','',''],
        ['17','yii\easyii\modules\news\models\News','1','First news H1','','',''],
        ['18','yii\easyii\modules\news\models\News','2','Second news H1','','',''],
        ['19','yii\easyii\modules\news\models\News','3','Third news H1','','',''],
        ['20','yii\easyii\modules\article\models\Category','1','Articles category 1 H1','Extended category 1 title','',''],
        ['21','yii\easyii\modules\article\models\Category','3','Subcategory 1 H1','Extended subcategory 1 title','',''],
        ['22','yii\easyii\modules\article\models\Category','4','Subcategory 2 H1','Extended subcategory 2 title','',''],
        ['23','yii\easyii\modules\article\models\Item','1','First article H1','','',''],
        ['24','yii\easyii\modules\article\models\Item','2','Second article H1','','',''],
        ['25','yii\easyii\modules\article\models\Item','3','Third article H1','','',''],
        ['26','yii\easyii\modules\gallery\models\Category','1','Album 1 H1','Extended Album 1 title','',''],
        ['27','yii\easyii\modules\gallery\models\Category','2','Album 2 H1','Extended Album 2 title','',''],
        ['28','yii\easyii\modules\catalog\models\Category','1','a','b','c','d'],
        ]);
        
        /* Table easyii_seotext_lang */
        $this->batchInsert('{{%easyii_seotext_lang}}',['id','owner_id','language','h1','title','keywords','description'],[['28','1','en-US','','EasyiiCMS demo aa','','yii2, easyii, admin bb'],
        ['29','1','zh-CN','','这是页面标题a','','这是页面描述b'],
        ['30','28','en-US','','','',''],
        ['31','28','zh-CN','','','',''],
        ['32','20','zh-CN','这是h1','','',''],
        ['33','20','en-US','this is h1','','',''],
        ['34','22','zh-CN','','','',''],
        ['35','22','en-US','','','',''],
        ['36','23','zh-CN','','','',''],
        ['37','23','en-US','','','',''],
        ['38','17','zh-CN','','','',''],
        ['39','17','en-US','','','',''],
        ['40','26','zh-CN','','','',''],
        ['41','26','en-US','','','',''],
        ['42','12','zh-CN','','','',''],
        ['43','12','en-US','','','',''],
        ['44','14','zh-CN','','','',''],
        ['45','14','en-US','','','',''],
        ]);
        
        /* Table easyii_shopcart_goods */
        $this->batchInsert('{{%easyii_shopcart_goods}}',['good_id','order_id','item_id','count','options','price','discount'],[]);
        
        /* Table easyii_shopcart_orders */
        $this->batchInsert('{{%easyii_shopcart_orders}}',['order_id','name','address','phone','email','comment','remark','access_token','ip','time','new','status'],[]);
        
        /* Table easyii_subscribe_history */
        $this->batchInsert('{{%easyii_subscribe_history}}',['history_id','subject','body','sent','time'],[]);
        
        /* Table easyii_subscribe_subscribers */
        $this->batchInsert('{{%easyii_subscribe_subscribers}}',['subscriber_id','email','ip','time'],[]);
        
        /* Table easyii_tags */
        $this->batchInsert('{{%easyii_tags}}',['tag_id','name','frequency'],[['10','php','1'],
        ['2','yii2','3'],
        ['3','jquery','3'],
        ['4','html','1'],
        ['7','ajax','1'],
        ]);
        
        /* Table easyii_tags_assign */
        $this->batchInsert('{{%easyii_tags_assign}}',['class','item_id','tag_id'],[['yii\easyii\modules\news\models\News','1','3'],
        ['yii\easyii\modules\news\models\News','1','2'],
        ['yii\easyii\modules\news\models\News','1','10'],
        ['yii\easyii\modules\news\models\News','2','2'],
        ['yii\easyii\modules\news\models\News','2','3'],
        ['yii\easyii\modules\news\models\News','2','4'],
        ['yii\easyii\modules\article\models\Item','2','2'],
        ['yii\easyii\modules\article\models\Item','2','3'],
        ['yii\easyii\modules\article\models\Item','2','7'],
        ]);
        
        /* Table easyii_texts */
        $this->batchInsert('{{%easyii_texts}}',['text_id','text','slug'],[['1','Welcome on EasyiiCMS demo website','index-welcome-title'],
        ]);
        
        /* Table easyii_texts_lang */
        $this->batchInsert('{{%easyii_texts_lang}}',['id','owner_id','language','text'],[['2','1','en-US','Welcome on EasyiiCMS demo website'],
        ['3','1','zh-CN','欢迎光临本站'],
        ]);
        
        $this->execute('SET foreign_key_checks = 1;');    
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->dropTable('{{%easyii_admins}}');
        $this->dropTable('{{%easyii_article_categories}}');
        $this->dropTable('{{%easyii_article_categories_lang}}');
        $this->dropTable('{{%easyii_article_items}}');
        $this->dropTable('{{%easyii_article_items_lang}}');
        $this->dropTable('{{%easyii_carousel}}');
        $this->dropTable('{{%easyii_carousel_lang}}');
        $this->dropTable('{{%easyii_catalog_categories}}');
        $this->dropTable('{{%easyii_catalog_categories_lang}}');
        $this->dropTable('{{%easyii_catalog_item_data}}');
        $this->dropTable('{{%easyii_catalog_items}}');
        $this->dropTable('{{%easyii_catalog_items_lang}}');
        $this->dropTable('{{%easyii_faq}}');
        $this->dropTable('{{%easyii_faq_lang}}');
        $this->dropTable('{{%easyii_feedback}}');
        $this->dropTable('{{%easyii_files}}');
        $this->dropTable('{{%easyii_files_lang}}');
        $this->dropTable('{{%easyii_gallery_categories}}');
        $this->dropTable('{{%easyii_gallery_categories_lang}}');
        $this->dropTable('{{%easyii_guestbook}}');
        $this->dropTable('{{%easyii_loginform}}');
        $this->dropTable('{{%easyii_modules}}');
        $this->dropTable('{{%easyii_modules_lang}}');
        $this->dropTable('{{%easyii_news}}');
        $this->dropTable('{{%easyii_news_lang}}');
        $this->dropTable('{{%easyii_pages}}');
        $this->dropTable('{{%easyii_pages_lang}}');
        $this->dropTable('{{%easyii_photos}}');
        $this->dropTable('{{%easyii_seotext}}');
        $this->dropTable('{{%easyii_seotext_lang}}');
        $this->dropTable('{{%easyii_settings}}');
        $this->dropTable('{{%easyii_shopcart_goods}}');
        $this->dropTable('{{%easyii_shopcart_orders}}');
        $this->dropTable('{{%easyii_subscribe_history}}');
        $this->dropTable('{{%easyii_subscribe_subscribers}}');
        $this->dropTable('{{%easyii_tags}}');
        $this->dropTable('{{%easyii_tags_assign}}');
        $this->dropTable('{{%easyii_texts}}');
        $this->dropTable('{{%easyii_texts_lang}}');
        $this->execute('SET foreign_key_checks = 1;');		    
    }
}
