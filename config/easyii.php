<?php

return [
    'modules' => [
        'admin' => [
            'class' => 'yii\easyii\AdminModule',
        ],
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<language:([a-zA-Z-_]{2,5})?>/admin/system/live-edit' => 'admin/system/live-edit',
                '<language:([a-zA-Z-_]{2,5})?>/admin/<controller:\w+>/<action:\w+>' => 'admin/<controller>/<action>',
                '<language:([a-zA-Z-_]{2,5})?>/admin/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => 'admin/<controller>/<action>',
                '<language:([a-zA-Z-_]{2,5})?>/admin/<module:\w+>/<controller:\w+>/<action:[\w-]+>' => 'admin/<module>/<controller>/<action>',
                '<language:([a-zA-Z-_]{2,5})?>/admin/<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => 'admin/<module>/<controller>/<action>',
            ],
         ],
        'user' => [
            'identityClass' => 'yii\easyii\models\Admin',
            'enableAutoLogin' => true,
            'authTimeout' => 86400,
        ],
        'i18n' => [
            'translations' => [
                'easyii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/messages',
                    'fileMap' => [
                        'easyii' => 'admin.php',
                    ]
                ]
            ],
        ],
        'formatter' => [
            'sizeFormatBase' => 1000
        ],
    ],
    'bootstrap' => ['admin']
];