<?php
return [
    'Catalog' => '产品目录',
    'Category fields' => '分类字段',
    'Manage fields' => '管理字段',

    'Add field' => '新增字段',
    'Save fields' => '保存字段',
    'Type options with `comma` as delimiter' => '各选项间用英文逗号作为分隔符',
    'Fields' => '字段',

    'Item created' => '添加成功',
    'Item updated' => '修改成功',
    'Item deleted' => '删除成功',

    'Title' => '型号',
    'Type' => '类型',
    'Options' => '选项',
    'Available' => '可用',
    'Price' => '价格',
    'Discount' => '优惠',
    'Select' => '下拉',
];