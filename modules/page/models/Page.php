<?php
namespace yii\easyii\modules\page\models;

use Yii;
use yii\easyii\behaviors\SeoBehavior;

use yeesoft\multilingual\behaviors\MultilingualBehavior;
use yeesoft\multilingual\db\MultilingualLabelsTrait;
use yii\easyii\components\MultilingualQuery;

class Page extends \yii\easyii\components\ActiveRecord
{
    use MultilingualLabelsTrait;

    public static function tableName()
    {
        return 'easyii_pages';
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            [['title', 'text'], 'trim'],
            ['title', 'string', 'max' => 128],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('easyii', 'Title'),
            'text' => Yii::t('easyii', 'Text'),
            'slug' => Yii::t('easyii', 'Slug'),
        ];
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    public static function findOne($id)
    {
        return self::find()->multilingual()->where(['page_id'=>$id])->one();;
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'attributes' => [
                    'title', 'text',
                ]
            ],
        ];
    }
}