<?php
namespace yii\easyii\modules\text\models;

use Yii;
use yii\easyii\behaviors\CacheFlush;

use yeesoft\multilingual\behaviors\MultilingualBehavior;
use yeesoft\multilingual\db\MultilingualLabelsTrait;
use yii\easyii\components\MultilingualQuery;

class Text extends \yii\easyii\components\ActiveRecord
{
    use MultilingualLabelsTrait;
    
    const CACHE_KEY = 'easyii_text';

    public static function tableName()
    {
        return 'easyii_texts';
    }

    public function rules()
    {
        return [
            ['text_id', 'number', 'integerOnly' => true],
            ['text', 'required'],
            ['text', 'trim'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique']
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => Yii::t('easyii', 'Text'),
            'slug' => Yii::t('easyii', 'Slug'),
        ];
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    public static function findOne($id)
    {
        return self::find()->multilingual()->where(['text_id'=>$id])->one();;
    }

    public function behaviors()
    {
        return [
            CacheFlush::className(),
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'attributes' => [
                    'text',
                ]
            ],
        ];
    }
}