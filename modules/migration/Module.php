<?php
namespace yii\easyii\modules\migration;

class Module extends \yii\base\Module
{
    public $migrationPath = "@app/migrations";
}
