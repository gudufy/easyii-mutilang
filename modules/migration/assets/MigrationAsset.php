<?php
namespace yii\easyii\modules\migration\assets;

class MigrationAsset extends  \yii\web\AssetBundle
{
    public $sourcePath = '@easyii/modules/migration/static';
    public $css = [
        'migration.css',
    ];
    public $js = [
        'migration.js'
    ];
    public $depends = [
        'yii\easyii\assets\AdminAsset',
    ];
}