<?php
return [
    'News' => '解决方案',
    'Create news' => '添加解决方案',
    'Edit news' => '修改解决方案',
    'News created' => '解决方案创建成功',
    'News updated' => '解决方案修改成功',
    'News deleted' => '解决方案删除成功',
    'Short' => '简述',
];